import { NgModule } from '@angular/core';
import { AuthService } from './services/Auth/auth.service';
import { DataClientService } from './services/DataClient/dataclient.service';
import { PoliciesService } from './services/Policies/policies.service';



@NgModule({
    imports: [],
    exports: [],
    declarations: [],
    providers: [
        AuthService,
        DataClientService,
        PoliciesService
    ],
})
export class CoreModule { }
