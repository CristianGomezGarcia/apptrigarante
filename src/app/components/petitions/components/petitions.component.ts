import { Component, OnInit } from '@angular/core';
import { getRootView } from '@nativescript/core/application';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';

@Component({
    selector: 'app-petitions',
    templateUrl: 'petitions.component.html',
    styleUrls: ['petitions.component.scss']
})

export class PetitionsComponent implements OnInit {

    titleActionBar: string = 'Mis Solicitudes';

    constructor() { }

    ngOnInit() { }

    showDrawer() {
        const sideDrawer = <RadSideDrawer>getRootView();
        sideDrawer.showDrawer();
    }
}