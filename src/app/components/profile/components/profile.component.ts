import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApplicationSettings } from '@nativescript/core';
import { getRootView } from '@nativescript/core/application';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import { ClientModel } from '~/app/core/Models/Client/Client@Model';
import { DataClientService } from '~/app/core/services/DataClient/dataclient.service';

@Component({
    selector: 'app-profile',
    templateUrl: 'profile.component.html',
    styleUrls: ['profile.component.scss']
})

export class ProfileComponent implements OnInit {

    titleActionBar: string = 'Mi Perfil';
    dataClientArr: ClientModel;
    userActive: number;

    constructor(
        private activatedRoute: ActivatedRoute,
        private dataClientService: DataClientService
    ) {
        /* this.dataClientArr = JSON.parse(this.activatedRoute.snapshot.params.dataclient); */
        this.userActive = ApplicationSettings.getNumber('userActive');
        this.getDataClient();
    }

    ngOnInit() { }

    showDrawer() {
        const sideDrawer = <RadSideDrawer>getRootView();
        sideDrawer.showDrawer();
    }

    getDataClient() {
        let arrayDataClient: ClientModel;
        this.dataClientService.getDataClient(this.userActive)
            .subscribe((data: ClientModel) => {
                arrayDataClient = data;
            }, error => {
                console.error('Error Profile Get Data Client => ', error.message);
            }, () => {
                this.dataClientArr = arrayDataClient;
            })
    }
}