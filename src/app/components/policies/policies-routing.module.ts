import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from '@nativescript/angular';
import { DetailsComponent } from './components/details/details.component';
import { PetitionsComponent } from './components/petitions/petitions.component';
import { PoliciesComponent } from './components/policies/policies.component';
import { ViewComponent } from './components/view/view.component';

const routes: Routes = [
    {
        path: '',
        component: PoliciesComponent
    },
    {
        path: 'view/:file',
        component: ViewComponent
    },
    {
        path: 'petitions',
        component: PetitionsComponent
    },
    {
        path: 'details/:itempolicie',
        component: DetailsComponent
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class PoliciesRoutingModule { }
