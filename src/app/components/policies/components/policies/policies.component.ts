import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { ApplicationSettings, Page } from '@nativescript/core';
import { getRootView } from '@nativescript/core/application';

import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import { PolicieModel } from '~/app/core/Models/Policie/Policie@Model';
import { PoliciesService } from '~/app/core/services/Policies/policies.service';

@Component({
    selector: 'app-policies',
    templateUrl: 'policies.component.html',
    styleUrls: ['policies.component.scss']
})

export class PoliciesComponent implements OnInit {

    titleActionBar: string = 'Mis Pólizas';
    dataLoaded: boolean = false;
    textLoader: string = 'Cargando Pólizas';
    userActive: number;
    policiesArr: Array<PolicieModel> = [];
    constructor(
        private routerExtensions: RouterExtensions,
        private policiesService: PoliciesService,
        private page: Page
    ) {
        this.userActive = ApplicationSettings.getNumber('userActive');
        this.getPolicies();
    }

    ngOnInit() { }

    showDrawer() {
        const sideDrawer = <RadSideDrawer>getRootView();
        sideDrawer.showDrawer();
    }

    getPolicies() {
        this.policiesService.getPolicies(this.userActive)
            .subscribe((data: PolicieModel[]) => {
                // console.log('data getPolicies() => ', data);
                this.policiesArr = data;
            }, error => {
                console.log(error.message);
            }, () => {
                this.dataLoaded = true;
                this.page.actionBarHidden = false;
                console.log('Length policiesArr => ', this.policiesArr.length);
            });
    }

    public onViewTap(file: string, policeNumer: string) {
        const paramsArr = JSON.stringify({ file: file, policieNumber: policeNumer });
        console.log('FileName => ', file);
        this.routerExtensions.navigate(['/policies/view', paramsArr], {
            transition: {
                name: 'fade'
            }
        });
    }

    public onPetitionsTap(anything: any) {
        this.routerExtensions.navigate(['/policies/petitions'], {
            transition: {
                name: 'fade'
            }
        });
    }

    public onDetailsTap(ItemPolicie: PolicieModel) {
        console.log('ItemPolice => ', ItemPolicie);
        this.routerExtensions.navigate(['/policies/details', JSON.stringify(ItemPolicie)], {
            transition: {
                name: 'fade'
            }
        });
    }

    public selectImage(alias: string) {
        const url: string = `~/assets/iconos-aseguradoras`;
        const images = [
            { alias: "ABA", name: "aba.png" },
            { alias: "SEGUROS AFIRME", name: "afirme.png" },
            { alias: "ANA SEGUROS", name: "ana.png" },
            { alias: "AXA", name: "axa.png" },
            { alias: "BANORTE", name: "banorte.png" },
            { alias: "GENERAL DE SEGUROS", name: "general.png" },
            { alias: "GNP", name: "gnp.png" },
            { alias: "HDI", name: "hdi.png" },
            { alias: "INBURSA", name: "inbursa.png" },
            { alias: "MAPFRE", name: "mapfre.png" },
            { alias: "MIGO", name: "migo.jpg" },
            { alias: "EL POTOSI", name: "elpotosi.png" },
            { alias: "QUALITAS", name: "qualitas.png" },
            { alias: "ZURA", name: "sura.png" },
            { alias: "ZURICH", name: "zurich.png" },
            { alias: "EL AGUILA", name: "elaguila.png" },
            { alias: "AIG", name: "aig.png" },
            { alias: "LA LATINO", name: "lalatino.png" }
        ];

        return `${url}/${images.find((e) => e.alias === alias).name}`;
    }

}