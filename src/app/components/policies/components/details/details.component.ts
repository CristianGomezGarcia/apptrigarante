import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from '@nativescript/angular';
import { PolicieModel } from '~/app/core/Models/Policie/Policie@Model';

@Component({
    selector: 'app-policies-details',
    templateUrl: 'details.component.html',
    styleUrls: ['details.component.scss']
})

export class DetailsComponent implements OnInit {

    titleActionBar: string = 'Detalles';
    policieDetailsArr: PolicieModel;

    constructor(
        private routerExtension: RouterExtensions,
        private activateRoute: ActivatedRoute
    ) {
        this.policieDetailsArr = JSON.parse(this.activateRoute.snapshot.params.itempolicie);
        this.policieDetailsArr.fechaRegistro = this.policieDetailsArr.fechaRegistro.substring(0, 10);
        this.policieDetailsArr.datos = JSON.parse(this.policieDetailsArr.datos);
        this.titleActionBar = `Póliza No.: ${this.policieDetailsArr.poliza}`;
    }

    ngOnInit() { }

    goBack() {
        this.routerExtension.backToPreviousPage();
    }
}