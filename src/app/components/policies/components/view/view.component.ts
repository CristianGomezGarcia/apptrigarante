import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from '@nativescript/angular';

import { Utils } from '@nativescript/core';

@Component({
    selector: 'app-view',
    templateUrl: 'view.component.html',
    styleUrls: ['view.component.scss']
})

export class ViewComponent implements OnInit {

    titleActionBar: string = 'Mi Póliza';
    PDF_URL: string;
    PDFComplete: boolean = false;

    constructor(
        private activateRoute: ActivatedRoute,
        private routerExtensions: RouterExtensions
    ) {
        const params = JSON.parse(this.activateRoute.snapshot.params.file);
        this.PDF_URL = `https://login-app.mark-43.net/auth-cliente/poliza/${params['file']}`;
        this.titleActionBar = `Póliza No.: ${params['policieNumber']}`;
    }

    ngOnInit() {
        console.log('PDF_URL => ', this.PDF_URL);
    }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }

    onLoad() {
        console.log('PDF Cargado correctamente');
        this.PDFComplete = true;
    }

    downLoadPolicie() {
        Utils.openUrl(this.PDF_URL);
        this.goBack();
    }
}