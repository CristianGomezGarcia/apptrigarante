import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { PetitionClass } from '~/app/core/Classes/Policies/petitions/Petition@Class';

@Component({
    selector: 'app-petitions-component',
    templateUrl: 'petitions.component.html',
    styleUrls: ['petitions.component.scss']
})

export class PetitionsComponent implements OnInit {

    petitionsArr: any[];
    isAccordionOpen: boolean = false;
    idToOpen: number = -1;

    petitionClass: PetitionClass = new PetitionClass();

    constructor(
        private routerExtensions: RouterExtensions
    ) { }

    ngOnInit() {
        this.petitionsArr = this.petitionClass.petitionsArr;
    }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }

    actionAccordion(id: number) {
        console.log(this.idToOpen);
        if (this.idToOpen === id) {
            if (this.isAccordionOpen) {
                this.isAccordionOpen = false;
            } else {
                this.isAccordionOpen = true;
            }
        } else {
            this.idToOpen = id;
            this.isAccordionOpen = true;
        }
    }

    callCenter() {
        alert("Llamando al callcenter");
    }

    // Instrucciones de pago
    private paymentInstructions() {
        console.log("Instrucciones de pago");
    }

    // Catálogo de facturacion
    private billingCatalgo() {
        console.log("Catálogo de facturación");
    }

    // Identificación oficial
    private officialIdentification() {
        console.log("Identificación oficial");
    }

    // Inspección vehicular
    private vehicleInspection() {
        console.log("Inspección vehicular");
    }

    // Aclaraciones y sugerencias
    private clarificationsAndSuggestions() {
        console.log("Aclaraciones y sugerencias");
    }

    // Cancelar póliza
    private cancelPolicy() {
        console.log("Cancelar póliza");
    }

    // Modificar póliza existente
    private modifyPolicy() {
        console.log("Modificar póliza");
    }

    // En caso de siniestro
    private eventOfClaim() {
        console.log("En caso de siniestro");
    }

}