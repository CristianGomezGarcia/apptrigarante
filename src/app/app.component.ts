import { Component, OnInit } from '@angular/core'
import { NavigationEnd, Router } from '@angular/router'
import { RouterExtensions } from '@nativescript/angular'
import {
  DrawerTransitionBase,
  RadSideDrawer,
  SlideInOnTopTransition,
} from 'nativescript-ui-sidedrawer'
import { filter } from 'rxjs/operators'
import { Application, ApplicationSettings } from '@nativescript/core'
import { DataClientService } from './core/services/DataClient/dataclient.service'
import { ClientModel } from './core/Models/Client/Client@Model'

@Component({
  selector: 'ns-app',
  templateUrl: 'app.component.html',
})
export class AppComponent implements OnInit {
  private _activatedUrl: string
  private _sideDrawerTransition: DrawerTransitionBase

  userActive: number;
  dataClientArr: ClientModel;

  constructor(
    private router: Router,
    private routerExtensions: RouterExtensions,
    private dataClientservice: DataClientService) {
    // Use the component constructor to inject services.
    /* this.userActive = ApplicationSettings.getNumber('userActive'); */
  }

  ngOnInit(): void {
    this._activatedUrl = '/policies'
    if (ApplicationSettings.getNumber('userActive')) {
      //this._sideDrawerTransition = new SlideInOnTopTransition()
    }

    this.router.events
      .pipe(filter((event: any) => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => (this._activatedUrl = event.urlAfterRedirects))

    /* this.getDataClient(); */
  }

  /* get sideDrawerTransition(): DrawerTransitionBase {
    return this._sideDrawerTransition
  } */

  isComponentSelected(url: string): boolean {
    return this._activatedUrl === url
  }

  onNavItemTap(navItemRoute: string): void {
    this.routerExtensions.navigate([navItemRoute], {
      transition: {
        name: 'fade',
      },
    })

    const sideDrawer = <RadSideDrawer>Application.getRootView()
    sideDrawer.closeDrawer()
  }

  getDataClient() {
    let arrayDataClient: ClientModel;
    this.dataClientservice.getDataClient(this.userActive)
      .subscribe((data: ClientModel) => {
        arrayDataClient = data;
      }, error => {
        console.error('Error Load User Data Client => ', error.message);
      }, () => {
        this.dataClientArr = arrayDataClient;
        console.info('La información se cargó correctamente');
      });
  }

}
