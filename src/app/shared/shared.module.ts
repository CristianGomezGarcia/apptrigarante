import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "@nativescript/angular";
import { FloatingButtonComponent } from "./components/floating-button/floating-button.component";
import { LoaderComponent } from "./components/loader/loader.component";

@NgModule({
    imports: [
        NativeScriptCommonModule
    ],
    exports: [
        LoaderComponent,
        FloatingButtonComponent
    ],
    declarations: [
        LoaderComponent,
        FloatingButtonComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class SharedModule { }
