import { NgModule } from '@angular/core'
import { Routes } from '@angular/router'
import { NativeScriptRouterModule } from '@nativescript/angular'
import { AuthGuard } from './guard/auth.guard'

const routes: Routes = [
  {
    path: '',
    redirectTo: '/policies',
    pathMatch: 'full'
  },
  {
    path: 'policies',
    canActivate: [AuthGuard],
    loadChildren: () => import('~/app/components/policies/policies.module').then(m => m.PoliciesModule)
  },
  {
    path: 'petitions',
    canActivate: [AuthGuard],
    loadChildren: () => import('~/app/components/petitions/petitions.module').then(m => m.PetitionsModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('~/app/components/profile/profile.module').then(m => m.ProfileModule)
  },
  {
    path: 'login',
    loadChildren: () => import('~/app/components/login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'logout',
    loadChildren: () => import('~/app/components/logout/logout.module').then(m => m.LogoutModule)
  }
]

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule],
})
export class AppRoutingModule { }
